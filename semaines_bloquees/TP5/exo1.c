#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

#define N 16
#define MAX 3

int nb_essais = 0 ;
int gameOver = 0 ;


int inWord(char c, char word[6]){
    for(int i = 0 ; i<5 ; i++){
        if(word[i] == c){
            return 1 ;
        }
    }
    return 0 ;
}

int nbLettreDansMot(char c, char word[6], int length){
    int nb = 0 ;
    for(int i = 0 ; i<length ; i++){
        if(word[i] == c){
            nb++;
        }
    }
    return nb;
}

void insereMot(char mot_choisis[6]){
    // On recupère le mot de l'utilisateur.
    printf("\n\n\n\nChoisissez un mot.\n\n\n\n");
    char mot_insere[6] ;
    scanf("%s", mot_insere);

    // On regarde les lettres bien placées : 
    char lettres_bien_placees[6];
    for(int i = 0 ; i<5 ; i++){
        if(mot_choisis[i] == mot_insere[i]) lettres_bien_placees[i] = mot_choisis[i];
        else lettres_bien_placees[i] = '-';
    }
    lettres_bien_placees[5] = '\0'; 

    // On regarde les lettres mal placées, càd
    // les lettres qui sont contenues dans le mot à deviner MAIS PAS dans les lettres bien placées.
    char lettres_mal_placees[6] = "\0";
    int j = 0;
    for(int i = 0 ; i<5 ; i++){
        char lettre = mot_insere[i];
        if(inWord(lettre, mot_choisis)){
            if(lettre != mot_choisis[i]){
                int nb_occurences_lettres = nbLettreDansMot(lettre, mot_choisis, 5) - nbLettreDansMot(lettre, lettres_mal_placees, j) - nbLettreDansMot(lettre, lettres_bien_placees,5);
                if(nb_occurences_lettres > 0){
                    lettres_mal_placees[j] = lettre;
                    j++;
                }
            }
        }
    }
    lettres_mal_placees[j] = '\0';
    nb_essais++;
    printf("\n%s\n\n%s  %s\n", mot_choisis, lettres_bien_placees, lettres_mal_placees);

    if(nb_essais >= MAX || !strcmp(mot_choisis, lettres_bien_placees)){
        gameOver = 1;
    }
}


int main()
{
    char tableau [N][6] = { "AGENT", "AGILE", "AIDER", "BADGE", "BIERE", 
                       "CABLE", "CACHE", "CACAO", "CARTE", "CLEFS",
                       "ECRAN", "GEEEK", "ISIMA", "MULOT", "PUCES", "VIDEO" };

    srand(time(NULL));
    int random = rand() % N;
    printf("Bienvenue sur MOTUS !!\n");
    while(!gameOver){
        //insereMot(tableau[random]);
        insereMot("LAPIN");
    }
    if(nb_essais == MAX){
        printf("\n\n\nPerdu... Vous avez épuisé toutes vos chances !\n");
    }else{
        printf("\n\n\nBravo, vous avez réussi au bout de %d essais !!\n", nb_essais);
    }
    return 0;
}
