#include <stdio.h>
#include <stdlib.h>
#define N 6+1

int TAB[N][N];

void initialiseTab(){
    for(int i = 0; i<N ; i++){
        for(int j = 0 ; j<N ; j++){
            TAB[i][j] = -1;
        }
    }
}

void afficheTAB(){
    for(int i = 0; i<N ; i++){
        for(int j = 0 ; j<N ; j++){
            if (TAB[i][j] != -1){
                printf("| %d ", TAB[i][j]);
            }
        }
        printf("\n");
    }
}

void Pascal(){
    TAB[0][0] = 1 ;
    for(int i = 1 ; i<N ; i++){
        TAB[i][0] = 1 ;
        TAB[i][i] = 1 ;
        for(int j = 1 ; j < i ; j++){
            TAB[i][j] = TAB[i-1][j-1] + TAB[i-1][j] ;
        }
    }
}


int main(){
    initialiseTab();
    Pascal();
    afficheTAB();
    return 0 ;
}