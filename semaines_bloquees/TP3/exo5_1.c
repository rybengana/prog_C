#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 500


void afficheCaracAscii(int n, int m){
    char c ;
    for(int i = n; i < m ; i++){
        c = i ;
        printf("%c\n", c);
    }
}

void caps(char string[N]){
    int i = 0 ;
    while(string[i] != '\0'){
        if(string[i] >= 'a' && string[i] <= 'z'){
            string[i] = 'A' + (string[i] - 'a');
        }
        i++;
    }
}

int main()
{
    //afficheCaracAscii(0, 150);     Impossible de les afficher dans le terminal.
    char s[N] ;
    scanf("%s", s);
    caps(s);
    printf("%s\n", s);
    return 0;

}
