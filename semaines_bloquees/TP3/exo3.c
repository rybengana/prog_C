#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MAX 100

int est ;
int n;

int askNumber(){
    int nb_choisi;
    int nb_tours = 0 ;
    do{
        printf("Choisissez un nombre !\n");
        scanf("%d", &nb_choisi);
        if(nb_choisi > n){
            printf("Trop haut !\n");
        } else if (nb_choisi < n){
            printf("Trop bas !\n");
        }
        nb_tours++;
    }while( (nb_choisi != n) && (est > nb_tours) );

    return (est == nb_tours && nb_choisi == n);   
}

int main(){
    srand(time(NULL));
    n = rand() % (MAX - 1) ;
    printf("Combien de tours ?\n");
    scanf("%d", &est);
    if(askNumber()){
        printf("Bravo\n") ;
    }else{
        printf("Perdu\n");
    }    
    return 0;
}