#include <stdio.h>


void afficheTab(int TAB[], int N){
    for(int i =0 ; i<N ; i++){
        printf("| %d ", TAB[i]);
    }
    printf("\n");
}


void initialiseTab(int TAB[], int N){
    for(int i =0 ; i<N ; i++){
        TAB[i] = 100 ;
    }
}

void addTab(int TAB[], int N){
    for(int i =0 ; i<N ; i++){
        if(i%2 == 0){
            TAB[i] = TAB[i] + i ;
        }else{
           TAB[i] = TAB[i] - i ; 
        }
    }
}

int minimumTab(int TAB[], int N){
    if(N > 0){
        int mini = TAB[0] ;
        for(int i =1 ; i<N ; i++){
            if (mini > TAB[i]) {
                mini = TAB[i];
            }
        }
        return mini;
    } else return -1 ;
}

int maximumTab(int TAB[], int N){
    if(N > 0){
        int maxi = TAB[0] ;
        for(int i =1 ; i<N ; i++){
            if (maxi < TAB[i]) {
                maxi = TAB[i];
            }
        }
        return maxi;
    } else return -1 ;
}

int sommeTab(int TAB[], int N){
    int somme = 0 ;
    for(int i =0 ; i<N ; i++){
        somme = somme + TAB[i];
    }
    return somme ;
}

float moyenne(int TAB[], int N){
    int somme = sommeTab(TAB, N);
    return (float) somme / N ;
}


int main (){

    int N = 10 ;
    int TAB[N];
    initialiseTab(TAB, N);
    addTab(TAB, N);
    afficheTab(TAB, N);

    printf("Minimum : %d\n", minimumTab(TAB, N));
    printf("Maximum : %d\n", maximumTab(TAB, N));
    printf("Moyenne : %f\n", moyenne(TAB, N));

    return 0 ;
}