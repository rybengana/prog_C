#include <stdio.h>
#include <stdlib.h>
#define N 25

 int fibonnacci_recursive(int n){
    if(n == 1){
        return 1 ;
    }else if (n == 0){
        return 0 ;
    }else if (n >= 2){
        return (fibonnacci_recursive(n - 1) + fibonnacci_recursive(n - 2));
    } else{
        exit(1);
    }
 }

 void afficheTab(int TAB[], int m){
    for(int i =0 ; i<m ; i++){
        printf("| %d ", TAB[i]);
    }
    printf("\n");
}


 void fibonnacci(int TAB[], int n){
    TAB[0] = 0 ;
    TAB[1] = 1 ;
    for(int i = 2 ; i<n ; i++){
        TAB[i] = TAB[i-1] + TAB[i-2];
    }
    afficheTab(TAB, n);
 }



int main(){
    int TAB[N+1];
    printf("Récursif : %d\n", fibonnacci_recursive(N));
    fibonnacci(TAB, N+1);
    return 0 ;
}
