#include <stdio.h>
#define N 10


void afficheTab(int tab[]){
    for(int i =0 ; i<N ; i++){
        printf("| %d ", tab[i]);
    }
    printf("\n");
}

int sommeTab(int tab[]){
    int somme = 0 ;
    for(int i =0 ; i<N ; i++){
        somme = somme + tab[i];
    }
    return somme ;
}

int main(){
    //Commande a taper : ./exo2 < entries.txt
    int TAB[N] ;
    int i ;
    for(i = 0; i<N ; i++){
        printf("La %dième valeur ?\n", i);
        scanf("%d", &TAB[i]);
    }
    afficheTab(TAB);
    printf("Somme : %d\n", sommeTab(TAB));
    return 0;
}

