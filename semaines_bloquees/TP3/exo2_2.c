#include <stdio.h>
#define N 10

int TAB[N] ;


void afficheTab(){
    for(int i =0 ; i<N ; i++){
        printf("| %d ", TAB[i]);
    }
    printf("\n");
}


void initialiseTab(){
    for(int i =0 ; i<N ; i++){
        TAB[i] = 100 ;
    }
}

void addTab(){
    for(int i =0 ; i<N ; i++){
        if(i%2 == 0){
            TAB[i] = TAB[i] + i ;
        }else{
           TAB[i] = TAB[i] - i ; 
        }
    }
}

int minimumTab(){
    if(N > 0){
        int mini = TAB[0] ;
        for(int i =1 ; i<N ; i++){
            if (mini > TAB[i]) {
                mini = TAB[i];
            }
        }
        return mini;
    } else return -1 ;
}

int maximumTab(){
    if(N > 0){
        int maxi = TAB[0] ;
        for(int i =1 ; i<N ; i++){
            if (maxi < TAB[i]) {
                maxi = TAB[i];
            }
        }
        return maxi;
    } else return -1 ;
}

int sommeTab(){
    int somme = 0 ;
    for(int i =0 ; i<N ; i++){
        somme = somme + TAB[i];
    }
    return somme ;
}

float moyenne(){
    int somme = sommeTab();
    return (float) somme / N ;
}


int main (){

    initialiseTab();
    addTab();
    afficheTab();

    printf("Minimum : %d\n", minimumTab());
    printf("Maximum : %d\n", maximumTab());
    printf("Moyenne : %f\n", moyenne());

    return 0 ;
}