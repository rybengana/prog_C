#include <stdio.h>

int global = 1;


void modifier_globale(){
    global = 2 ;
}

int main()
{
    printf("%d\n", global);
    modifier_globale();
    printf("%d\n", global);
    return 0;
}
