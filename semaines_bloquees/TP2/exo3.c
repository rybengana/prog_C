#include <stdio.h>


long int factorielle(int n){
    long int facto = 1 ;
    for(int i = 2; i<=n; i++){
        facto *= i;
    }
    return facto;
}

long int recursive_factorielle(int n){
    if(n == 1 || n == 0){
        return 1 ;
    } else if (n >= 2){
        return (recursive_factorielle(n-1) * n);
    } else{
        return 0 ;
    }
}

int main(){
    int n = 56 ;
    long int facto = factorielle(n);
    long int facto_recursif = recursive_factorielle(n);
    printf("factorielle de %d : %ld\n", n, facto);
    printf("factorielle récursif de %d : %ld\n", n, facto_recursif);
    if (facto == facto_recursif){
        printf("c'est bon !\n");
    } else{
        printf("c'est pas bon!\n");
    } 
    return 0;
}