#include <stdio.h>
#include <stdlib.h>


int main (){

    int a;
    printf("Choisissez un nombre!\n");
    scanf("%d", &a);

    int somme_choisie;
    printf("CHOIX DE LA SOMME :\n\t 1) Somme des n.\n\t 2) Somme des n carrés\n\t 3) Somme des 1 / n carrés\n");
    scanf(" %d", &somme_choisie);

    int i ;
    switch (somme_choisie)
    {
    case 1:
        int somme1 = 0;
        for(i=0; i<=a ; i++){
            somme1 += i;
        }
        printf("Somme des n : ");
        printf(" %d\n", somme1);
        break;
    case 2:
        int somme2 = 0;
        for(i=0; i<=a ; i++){
            somme2 += i*i;
        }
        printf("Somme des n carrés : ");
        printf(" %d\n", somme2);
        break;
    case 3:
        float somme3 = 0;
        for(i=1; i<=a ; i++){
            somme3 += (float) 1 / (i*i);
        }
        printf("Somme des 1 / n carré : ");
        printf(" %f\n", somme3);
        break;
    default:
        printf("Veillez entrer une réponse valide.\n");
        exit(EXIT_FAILURE);
        break;
    }
    

    return 0;
    
}