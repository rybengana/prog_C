#include <stdio.h>

void modifier_local(int local){
    local = 2 ;
    printf("local dans fonction : %d\n", local);
}

int main()
{
    int local = 1 ;
    printf("%d\n", local);
    modifier_local(local);
    printf("%d\n", local);
    return 0;
}