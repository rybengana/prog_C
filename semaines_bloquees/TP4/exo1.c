#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#define N 300


char mot_a_trouver[N] = "PIPOU";
char mot_decouvert[N] ;
char cartoon [10][70] = {
  "\n\n\n\n\n\n_______\n",
  "\n   |\n   |\n   |\n   |\n   |\n___|___\n",
  "\n   /-----\n   |\n   |\n   |\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |\n   |\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |    |\n   |   \n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |   /|\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |   /\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |   /|\n___|___\n"
    };

int nb = -1 ;


void cacher(){
    int i = 0;
    while(mot_a_trouver[i] != '\0'){
        mot_decouvert[i] = '*';
        i++;
    }
    mot_decouvert[i] = '\0' ;
}


bool inWord(char c, char str[N]){
    int i=0; 
    while(str[i] != '\0'){
        if(str[i] == c){
            return true;
        }
        i++;
    }
    return false;
}

void afficheCurrentState(){
    if(nb >= 0){
        printf("%s\n", cartoon[nb]);
    }
    printf("\n\n\n");
    printf("MOT A DECOUVRIR :\n%s\n", mot_decouvert);
}


void lettre_donnee(char c){
    if(inWord(c, mot_a_trouver)){
        int i = 0 ;
        while(mot_decouvert[i] != '\0'){
            if(mot_a_trouver[i] == c){
                mot_decouvert[i] = c ;
            }
            i++;
        }
    }else{
        nb++;
    }
}


int main()
{
    bool over = false ;
    char c_choisi;
    cacher();
    while(over == false){
        afficheCurrentState();
        scanf("%c%*c", &c_choisi);
        if(isalpha(c_choisi)){
            lettre_donnee(toupper(c_choisi));
            if(nb == 10){
                over = true;
                printf("Perdu....\n%s\n", mot_a_trouver);
            }else if(strcmp(mot_a_trouver, mot_decouvert) == 0){
                printf("%s\nBravo, gagné !\n", mot_a_trouver);
                over = true;
            }
        } else{
            over = true;
        }
        
    }    
    return 0;
}
