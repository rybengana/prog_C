/*
     Voici un premier programme en C qui correspond au "Hello World" classique
*/

#include <stdio.h>

int main()
{
   printf("Bonjour les ZZ1!\n");
   printf("Comment allez-vous ?\n");
   int i = -3;
   printf("%x\n", i);

   return 0;
}