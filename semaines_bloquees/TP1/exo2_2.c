#include <stdio.h>
#include <math.h>

float a = 1;
float b = 2;
float c = 1;

int main(){

    float delta = b*b - 4*a*c;

    if(delta > 0){
        printf("2 solutions :\n");
        float s1 = (-b + sqrt(delta)) / (2*a) ;
        float s2 = (-b - sqrt(delta)) / (2*a) ;
        printf("s1 = %f | s2 = %f\n", s1, s2);
    }else if (delta == 0){
        printf("1 solution double.\n");
        float s = (-b) / (2*a);
        printf("s = %f\n", s);
    }else{
        printf("Pas de solutions.\n");
    }

    return 0;
}