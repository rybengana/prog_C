#include <stdio.h>

int main (){

    printf("Tables de multiplication\n");

    for(int i = 1; i<= 12; i++){
        printf("%d : ", i);
        for(int j =1; j<=10; j++){
            printf("%d | ", j*i);
        }
        printf("\n");
    }
    return 0;
}