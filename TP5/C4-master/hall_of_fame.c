#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hall_of_fame.h"


// Ecrit dans le flux les données d'un joueur (alias, score sur un jeu, nom du jeu)
void afficherDonnee(FILE * file, donnee_t d) {
   fprintf(file, "%s : %s avec %d\n", d.jeu, d.alias, d.score);
}

void fgets2(char * dest, int MAX_LENGTH, FILE * buffer_file){
   int i = 0;
   char c;
   while(i < MAX_LENGTH && !( feof(buffer_file))){
      c = fgetc(buffer_file);
      if(c == '\n'){
         dest[i] = '\0';
         i = MAX_LENGTH;
      }else{
         dest[i] = c;
         i++;
      }
   }
}

// un petit commentaire ?
void saisirDonnee(FILE *file, donnee_t * p){
   char score[50];
   fgets2(p->jeu, 100, file);
   fgets2(p->alias, 40, file);
   fgets2(score, 50, file);
   p->score = atoi(score);
}

int tableauFromFilename(char * name, donnee_t T[TAILLE_MAX]){
   int i = 0 ;
   FILE * file = fopen(name, "r");
   if(file != NULL){
      while( !(feof(file)) && i < TAILLE_MAX){
         saisirDonnee(file, &T[i]);
         i++;
      }
   }
   //fclose(file);
   return i;
}

void tableauInFilename(char * name, donnee_t T[TAILLE_MAX], int nb){
   FILE * file = fopen(name, "w");
   if(file != NULL){
      for(int i = 0 ; i < nb ; i++){
         afficherDonnee(file, T[i]);
      }
   }
   fclose(file);
}