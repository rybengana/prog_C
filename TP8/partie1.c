#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define N 255

typedef struct cellule 
{
  char ligne[N];
  struct cellule * suiv;
} cellule_t;

typedef struct liste 
{
  cellule_t * tete;
  cellule_t * fin;
} liste_t;


void ajouteFin(liste_t * l, cellule_t * elem){
    if(l->tete == NULL){
        l->tete = elem;
    }else{
        l->fin->suiv = elem;
    }
    l->fin = elem;
}


void afficheListe(liste_t *liste){
    cellule_t * elem = liste->tete;
    while(elem != NULL){
        fprintf(stdout, "%s\n", elem->ligne);
        elem = elem->suiv;
    }
}


void libererMemoireListe(liste_t * liste){
    cellule_t * elem = liste->tete;
    cellule_t * suiv ;
    while(elem != NULL){
        suiv = elem->suiv ;
        free(elem);
        elem = suiv ;
    }
    free(liste);
}


void exo(FILE * flux){
    char s[N] ;
    liste_t * liste = (liste_t *) malloc(sizeof(liste_t)) ;
    
    if(liste == NULL){
        exit(-1);
    }
    while(!feof(flux))
    {
        fgets(s, N, flux);
        s[strcspn(s, "\n")] = '\0';
        cellule_t * elem = (cellule_t *) malloc(sizeof(cellule_t)) ;
        if(elem == NULL){
            exit(-1);
        }
        elem->suiv = NULL;
        strcpy(elem->ligne, s);
        ajouteFin(liste, elem);
    }
    afficheListe(liste);
    libererMemoireListe(liste);
}


int main(int argc, char ** argv)
{  
    if(argc == 2){
        FILE * file = fopen(argv[argc - 1], "r");
        exo(file);
        fclose(file);
    }else{
        exo(stdin);
    }
    
    return 0;
}