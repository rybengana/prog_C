#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define TAILLE 12

#define COULEURS 6 
#define ESSAIS_MAX 22


//Regular text
#define BLK "\e[0;30m"
#define RED "\e[0;31m"
#define GRN "\e[0;32m"
#define YEL "\e[0;33m"
#define BLU "\e[0;34m"
#define MAG "\e[0;35m"
#define CYN "\e[0;36m"
#define WHT "\e[0;37m"


#define COLOR_RESET "\e[0m"
char * tab_couleurs[6] = {RED, BLK, GRN, YEL, BLU, MAG};

void afficherGrille(int ** t){
    for(int i  = 0 ; i < TAILLE ; i++){
        for(int j = 0 ; j < TAILLE ; j++){
            printf("%s%d ", tab_couleurs[t[i][j] % 6], t[i][j]);
        } 
        printf("%s\n", COLOR_RESET);
    } 
} 

void initialiser(int ** t){
    srand(time(NULL));
    for(int i  = 0 ; i < TAILLE ; i++){
        for(int j = 0 ; j < TAILLE ; j++){
           t[i][j] = rand() % COULEURS;
        } 
    } 
} 

void remplir_recursive(int ** t, int i, int j, int ancienne_couleur, int nouvelle_couleur){
    if(ancienne_couleur != nouvelle_couleur){
        if(i < TAILLE && i >= 0 && j < TAILLE && j >= 0){
            if(t[i][j] == ancienne_couleur){
                t[i][j] = nouvelle_couleur;
                if (i > 0) remplir_recursive(t, i-1, j, ancienne_couleur, nouvelle_couleur);
                if (i < TAILLE - 1) remplir_recursive(t, i+1, j, ancienne_couleur, nouvelle_couleur);
                if (j > 0) remplir_recursive(t, i, j-1, ancienne_couleur, nouvelle_couleur);
                if (j < TAILLE - 1) remplir_recursive(t, i, j+1, ancienne_couleur, nouvelle_couleur);
            }
        }
    } 
}



int finit(int ** t){
    int couleur = t[0][0] ;
    for(int i = 0 ; i<TAILLE ; i++){
        for(int j = 0 ; j<TAILLE ; j++){
            if(t[i][j] != couleur){
                return 0 ;
            }
        }
    }
    return 1 ;
}


void lib(int ** t){
    for(int i = 0 ; i<TAILLE ; i++){
        free(t[i]);
        t[i] = NULL;
    }
    free(t) ;
}


int main() {

    int ** t = (int **) malloc(sizeof(int *) * TAILLE);
    for(int i = 0 ; i < TAILLE ; i++){
        t[i] = (int *) malloc(sizeof(int) * TAILLE);
    }

    int nb_coups = 0 ;
    initialiser(t);

    while( !(finit(t)) && nb_coups < ESSAIS_MAX){
        afficherGrille(t);
        printf("\n\nChoisissez un nombre !\n(nb essais : %d / %d)\n", nb_coups, ESSAIS_MAX);
        int couleur ;
        scanf("%d", &couleur);
        remplir_recursive(t,0,0,t[0][0],couleur);
        nb_coups++;
    }
    afficherGrille(t);
    printf("\n\n\nBravo vous avez gagné en %d essais !\n", nb_coups);
    lib(t);
    return 0 ;
}