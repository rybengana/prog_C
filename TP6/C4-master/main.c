#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/*
    A FINIR A CAUSE DE SAMOUEL
*/


typedef struct donnee{
    int score ;
    char jeu[100] ;
    char alias[40];
    struct donnee * suivant ;
} donnee_t;




void affiche(donnee_t * d){
    printf("\nscore : %d\nJeu : %s\nNom : %s\n", d->score, d->jeu, d->alias);
}

void afficheListe(donnee_t * tr){
    int nb = 0 ;
    while (tr != NULL){
        printf("\nElement n°%d", nb);
        affiche(tr);
        tr = tr->suivant;
        nb++;
    }
}



void ajouteFinListe(donnee_t * d, donnee_t * tr){
    while(tr->suivant != NULL){
        tr = tr->suivant;
    }
    tr->suivant = d ;
    d->suivant = NULL;
}

void ajouteTete(donnee_t * d, donnee_t * tr, donnee_t ** t){
    d->suivant = tr ;
    *t = d ;
}

/*
void libererMemoire(donnee_t * tr){
    if(tr->suivant == NULL){
        free(tr);
    }
    else{
        if(tr->suivant->suivant == NULL){
            free(tr->suivant);
            tr->suivant = NULL;
        }else{
            libererMemoire(tr->suivant);
            libererMemoire(tr);
        }
    }
}
*/

void libererMemoire(donnee_t * tr){
    while(tr != NULL){
        donnee_t * tmp = tr->suivant ;
        tr->suivant = NULL;
        free(tr);
        tr = tmp;
    }
}


int main(){

    donnee_t * tr = malloc(sizeof(donnee_t)) ;
    tr->score = 5 ;
    strcpy(tr->alias, "Loucasse");
    strcpy(tr->jeu, "Minecraft");

    donnee_t * fin = malloc(sizeof(donnee_t));
    fin->score = 10 ;
    strcpy(fin->alias, "Samouel");
    strcpy(fin->jeu, "Minecraft");  
    //tr->suivant = fin ;

    donnee_t * deb = malloc(sizeof(donnee_t));
    deb->score = 1 ;
    strcpy(deb->alias, "Robinne");
    strcpy(deb->jeu, "Minecraft");  
    //deb->suivant = tr;
    //tr = deb ;
    ajouteTete(fin, tr, &tr);
    ajouteTete(deb, tr, &tr);
    afficheListe(tr);
    libererMemoire(tr);
    return 0;
}