#include<stdio.h>

#include "code.h"
#include "teZZt.h"

/*
TEST(identification_avec_indice) {
  CHECK(  0 == identification("x"));
  CHECK( -1 == identification("x\n"));
  CHECK(  1 == identification("sin(x)"));
  CHECK(  2 == identification("cos(x)"));
  CHECK(  3 == identification("log(x)"));
  CHECK(  4 == identification("exp(x)"));
  CHECK( -1 == identification("t"));
}


TEST(identification_avec_enum) {
  CHECK(   ID == identification("x"));
  CHECK( NONE == identification("x\n"));
  CHECK(  SIN == identification("sin(x)"));
  CHECK(  COS == identification("cos(x)"));
  CHECK(  LOG == identification("log(x)"));
  CHECK(  EXP == identification("exp(x)"));
  CHECK( NONE == identification("t"));
}
*/


int main() {
	// Tester avec evalp !!

	printf("Quelle fonction à utiliser?\n");
	char function[10];
	OP f;

	scanf("%s", function);
	f = identification(function);
	if(f == -1)
	{
		printf("ERREUR NOM DE FONCTION\n");
		return 0;
	}
	printf("L'interval ?\n");

	double inter[2];
	scanf("%lf", &inter[0]);
	scanf("%lf", &inter[1]);
	printf("Progression?\n");

	double delta;
	scanf("%lf", &delta);
	printf("Fichier dans lequel écrire?\n");
	char fichier[100];
	scanf("%s", fichier);

	calcul(inter, delta, f, fichier);

 	return 0;
}
