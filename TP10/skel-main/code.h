
#ifndef GARDIEN_UNIQUE_CODE_H
#define GARDIEN_UNIQUE_CODE_H

// fichier d'entete classique ou l'on declare les types necessaires, 
// les fonctions implementees dans le fichier .c
// les declaration de variables globales externes

    #include<string.h>
    #include<stdio.h>
    #include<stdlib.h>
    #include<math.h>

    typedef enum ope {
    NONE = -1,
    ID = 0,
    SIN = 1,
    COS = 2, 
    LOG = 3, 
    EXP = 4
    } OP;

    OP identification(char *);
    double evalf(double, OP);
    void calcul(double *, double, OP, char *);

#endif
