#include "code.h"

// implementation des fonctions declarees dans le fichier .h
// definition eventuelle des variables globales s'il y en a


const char * OPER_NAMES[] = { "x", "sin(x)", "cos(x)", "log(x)", "exp(x)", NULL }; 
const double (*OPER_FN [])(double) = { identite, sin, cos, log, exp, erreur };


OP identification(char * fonction){
    int i = 0;
    while(OPER_NAMES[i] != NULL){
        if (strcmp(fonction, OPER_NAMES[i]) == 0)
        {
            return (OP)i;
        }
        i++;
    }
    return NONE;
}   


double erreur(double val){
    fprintf(stderr, "ERREUR : NOM DE FONCTION INCORRECT");
    return NAN;
}


double identite(double val){
    return val;
}

double evalf(double val, OP function){
    double return_value ;
    switch (function)
    {
    case 0:
        return_value = val;
        break;
    case 1:
        return_value = sin(val);
        break;
    case 2:
        return_value = cos(val);
        break;
    case 3:
        return_value = log(val);
        break;
    case 4:
        return_value = exp(val);
        break;
    default:
        return_value = NAN;
        break;
    }
    return return_value;
}

double evalp(double val, OP function)
{
    return OPER_FN[(int) function](val);
}

void calcul(double inter[], double delta, OP function, char * file_name){

    FILE * file = fopen(file_name, "w");
    if(file == NULL){
        exit(-1);
    }

    for(double val = inter[0]; val < inter[1]; val+=delta)
    {
        fprintf(file, "val : %lf | result : %lf\n", val, evalf(val, function));
    }
   
    fclose(file);
}