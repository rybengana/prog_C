#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define NB 10

int guichet_A;
int guichet_B;
int guichet_C;
int guichet_D;
int guichet_E;
int guichet_F;
int guichet_G;
int guichet_H;
int guichet_I;
int guichet_J;

void initialisation(){
    guichet_A = 0 ;
    guichet_B = 0 ;
    guichet_C = 0 ;
    srand(time(NULL));
    int r = rand() % 3 ;
    switch(r){
        case 0 :
            guichet_A = 1 ;
            break;
        case 1 :
            guichet_B = 1 ;
            break;
        case 2 :
            guichet_C = 1 ;
            break;
    }
}


void initialisation2(int * t[NB]){
    for(int i = 0 ; i < NB ; i++){
        *(t[i]) = 0;
    }
    srand(time(NULL));
    int r = rand() % 10 ;
    *(t[r]) = 1;
}

void affichage(int * t[NB]){
    for(int i = 0 ; i < NB ; i++){
        printf("%d | ", *(t[i]));
    }
    printf("\n");
}

int main (){
    int * t[NB] ;
    t[0] = &guichet_A;
    t[1] = &guichet_B;
    t[2] = &guichet_C;
    t[3] = &guichet_D;
    t[4] = &guichet_E;
    t[5] = &guichet_F;
    t[6] = &guichet_G;
    t[7] = &guichet_H;
    t[8] = &guichet_I;
    t[9] = &guichet_J;
    initialisation2(t);
    //printf("%d %d %d \n", guichet_A, guichet_B, guichet_C);
    affichage(t);
    return 0;
}

