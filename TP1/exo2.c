#include <stdio.h>
#define N 9
#define VERT 1
#define HORIZ 0


int verifierLigneColonne(int grille[][N], int sens, int numero){
    int retour = 0 ;
    if(i < N){
        int tab[N] ;
        for(int k=0 ; k<N ; k++){
            if(sens == VERT){
                grille[k][numero]  
            }else if (sens == HORIZ){
                grille[numero][k]   
            }  
        }  
    } 
    return retour;
} 


int saisir(int grille[][N]){
    int i;
    int j;
    int valeur;
    int retour = 0 ;
    printf("Ligne?\n");
    scanf("%d", &i);
    printf("Colonne?\n");
    scanf("%d", &j);
    printf("Valeur?\n");
    scanf("%d", &valeur);
    if(grille[i][j] == 0){
        grille[i][j] = valeur;
        retour++;  
    } 
    return retour ;
} 

void initialiser(int grille[][N]){
    for(int i=0 ; i<N ; i++){
        for(int j=0 ; j<N ; j++){
            grille[i][j] = 0; 
        } 
    } 
} 

void afficher(int grille[][N]){
    for(int i=0 ; i<N ; i++){
        for(int j=0 ; j<N ; j++){
            if (grille[i][j] != 0) printf("%d ", grille[i][j]);
            else printf("* ");
        }
        printf("\n"); 
    } 
}  

int generer(int grille[][N]){
    int i, j;
    int nb = 0 ;
    for(j=0;j<N; ++j) 
    {
        for(i=0; i<N; ++i){
            if((i+j) % 2 == 1){
                grille[j][i] = (i + j*3 +j /3) %N +1 ; 
                nb++;
            }  
        } 
    }
    return nb;
} 

int main (){
    printf("SUDOKU :\n\n");
    int grille[N][N];
    int remplissage = 0;
    initialiser(grille);
    afficher(grille);
    remplissage = generer(grille);
    printf("Remplissage : %d\n\n", remplissage);
    afficher(grille);
    remplissage = saisir(grille);
    afficher(grille);
    return 0 ;
} 