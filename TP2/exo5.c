#include <stdio.h>
#include <stdlib.h>


int main(){
    double * tab  = (double *) malloc(sizeof(double) * 1000) ;
    for(int i = 0; i < 1000 ; i++){
        *(tab+i) = i*i ;
    } 
    for(int j = 0; j < 1000 ; j++){
        printf("%lf ", *(tab)+j);
    } 
    free(tab) ;
    tab = NULL;
    return 0;
} 