#include <stdio.h>

int compter1(char * chaine) 
{   
  int i = 0;

  while (*(chaine+i) != '\0')
     ++i;

  return i; 
}

int compter2(char * chaine) 
{   
  char * s = chaine;

  while (*chaine != '\0')
     ++chaine;

  return chaine - s; 
}

int compter3(char * chaine) 
{   
  char * s = chaine;

  while (*chaine++); // S'arrete lorsque le pointeur '0' est NULL, donc quand on sort de la chaine (\0)

  return chaine - s; 
}


int main()
{
//    int tab[] = {0,1,2,3,4,5};

//    printf("%lu %lu %lu\n", sizeof(char), sizeof(int), sizeof(double));

//    int  * p1;
//    char * p2;

//    p1 = tab; 
//    ++p1;
//    printf("%d\n", *p1);

//    p2 = (char *) p1;
//    p2 += sizeof(int);

//    printf("%d", *((int*)p2));
//    printf("%d", *(p1+6));

//    p1 = NULL;
   //printf("%d", *p1);
   char s[5] = "abcd" ;
   printf("1 : %d | 2 : %d | 3 : %d\n", compter1(s), compter2(s), compter3(s)); 

   return 0;
}