#include <stdio.h>



void echangeValeur(int a, int b){
    printf("a = %d et b = %d\n", a, b);
    int c = a ;
    a = b ;
    b = c ;
    printf("a = %d et b = %d\n", a, b);
}  

void echangeParAdresse(int *a, int *b){
    printf("*a = %d et *b = %d\n", *a, *b);
    int c = *a;
    *a = *b ;
    *b = c ;
    printf("*a = %d et *b = %d\n", *a, *b);
} 

int main(){
    int i = 1 ;
    int j = 3 ;
    printf("i = %d et j = %d\n", i, j);
    //echangeValeur(i, j);
    echangeParAdresse(&i, &j);
    printf("i = %d et j = %d\n", i, j);
    return 0;
} 