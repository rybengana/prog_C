#include<stdio.h>

int main()
{
   double d = 4.2, *ptrd = &d;
   int i  = 3, *ptri  = &i;
   char c1 = '1', *ptrc1 = &c1;
   char c2 = '2', *ptrc2 = &c2;

   printf("ptri = %p ptrc1 = %p \n",ptri, ptrc1);
   printf("ptri = %p ptrc1 = %p \n",ptri, ptrc1);
   // seule la version suivante ne genere pas d'avertissement
   printf("ptri = %p ptrc1 = %p \n",ptri, ptrc1);
   printf("*ptri = %d et *ptrc1=%c\n", *ptri, *ptrc1);
   
   ptri++;
   //ptrc1++;
   
   printf("ptri = %p ptrc1 = %p \n", &ptri, &ptrc1);
   // cela permet de voir la taille d'un int et d'un char en memoire
   // sizeof(int)  sizeof(char)
   printf("ptrd = %p\n", ptrd);
   ptrd += 2 ;
   printf("ptrd = %p\n", ptrd);
   printf("*ptrc1 = %c et *ptrc2 = %c\n", *ptrc1, *ptrc2);
   char tmp = *ptrc1 ;
   c1 = *ptrc2 ;
   c2 = tmp ;
   printf("*ptrc1 = %c et *ptrc2 = %c\n", *ptrc1, *ptrc2);
   return 0 ;
}