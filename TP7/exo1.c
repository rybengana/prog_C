#include </usr/include/SDL2/SDL.h>
#include </usr/include/SDL2/SDL_image.h>
#include </usr/include/SDL2/SDL_ttf.h>

/* VARIABLES GLOBALES */
SDL_Window   * window   = NULL; // fenetre utilisee par le programme
SDL_Renderer * renderer = NULL; // moteur de rendu SDL
SDL_Texture  * logo     = NULL; // image chargee
TTF_Font     * font1    = NULL; // polices de caracteres
TTF_Font     * font2    = NULL;
int            width    = 870;  // largeur de la fenetre
int            height   = 480;  // hauteur de la fenetre
SDL_Texture  *avatar;


void afficherFenetre() 
{

   SDL_Rect rect;
   int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    // images JPG ou PNG :-)
    int initted= 0;

    initted = IMG_Init(flags);

    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }

   /* on prépare/efface le renderer */
   SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
   SDL_RenderClear(renderer);
    
   /* dessiner en blanc */
   /*
   SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
   rect.x = rect.y = 0;
   rect.w = rect.h = 600;
   SDL_RenderFillRect(renderer, &rect );*/

   /* afficher le renderer dans la fenetre */
    rect.x = 300;
    rect.y = 110;
    rect.w = rect.h = 300;
    SDL_RenderCopy(renderer, avatar, NULL, &rect); 
    SDL_RenderPresent(renderer);
}


int main(){
    int running = 1 ;
    SDL_Event      event;
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }

    window = SDL_CreateWindow("SDL2 Programme 0.1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
                width, height, 
                SDL_WINDOW_RESIZABLE); 
        
    if (window == 0) 
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        /* on peut aussi utiliser SLD_Log() */
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
    if (renderer == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }
    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    // images JPG ou PNG :-)
    int initted= 0;

    initted = IMG_Init(flags);

    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }

    
    SDL_Rect rect;

    SDL_Surface *image = NULL;
    image=IMG_Load("loic.png");
    /* image=SDL_LoadBMP("loic.bmp"); // fonction standard de la SDL2 */
    if(!image) {
        printf("IMG_Load: %s\n", IMG_GetError());
    }

    avatar = SDL_CreateTextureFromSurface(renderer, image);
    SDL_FreeSurface(image);

    /*
    while (running) {

	while (SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_WINDOWEVENT:
				printf("window event\n");
				switch (event.window.event)  
				{
					case SDL_WINDOWEVENT_CLOSE:  
						printf("appui sur la croix\n");	
						break;
					case SDL_WINDOWEVENT_SIZE_CHANGED:
						width = event.window.data1;
						height = event.window.data2;
						printf("Size : %d%d\n", width, height);
					default:
						afficherFenetre();
				}   
			    break;
			case SDL_MOUSEBUTTONDOWN:
				printf("Appui :%d %d\n", event.button.x, event.button.y);
				// afficherEcran() ?
				break;
			case SDL_QUIT : 
				printf("on quitte\n");    
				running = 0;
		    }
	    }	
	SDL_Delay(1); //  delai minimal
    }*/
    afficherFenetre();
    SDL_Delay(5000);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_DestroyTexture(avatar);
    IMG_Quit();
    SDL_Quit();
    return 0 ;
}




