#ifndef histo_h


#define histo_h

#include"liste.h"
#include"commun.h"
#include<X11/Xlib.h>
#include<X11/Xutil.h>

#define HISTOSIZE 21

typedef struct gdata_s {
    Window         root;       
    Window         win;           
    Display       *dpy;    
    int            ecran;        
    GC             gcontext;         
    XGCValues      gcv;              
    Visual        *visual;
    Colormap       colormap;
    Font           font;
} gdata_t;

typedef int histogram_t[HISTOSIZE];

void computeHisto(histogram_t h, list_t l);
void displayHisto(histogram_t h);
int maxHisto(histogram_t h);
float meanHisto(histogram_t h);
int countHisto(histogram_t h);
void displayGraphicalHisto(gdata_t g, histogram_t h);
void displayGraph(histogram_t h);
void displayText(histogram_t h);

#endif