#ifndef commun_h
#define commun_h

#include<stdio.h>
#include<stdlib.h>
#include<string.h>


#ifdef DEBUG
    #define LOG(A) printf A
#else
    #define LOG(A) 
#endif

#ifndef ERROR_OK
    #define ERROR_OK          0
    #define ERROR_LIST_ALLOC  1
    #define ERROR_FILE        1
#endif

#endif